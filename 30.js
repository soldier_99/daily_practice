// Async using calllback


let stocks = {
    Fruits : {Banana: 10, Grapes: 50, Orange:3, Mango:23},
    liquid : {Ice: Infinity,Milk:73},
    holders : {Cup:40,Stick:200,Cone:65},
    toppings : {Chocoloates: 40, Peanuts: 32, Gems:203, "Dry Fruits":302}
    
};
console.log(stocks);

console.log("Welcome, When we say we provide best Ice Creams, we Mean it.. ");
setTimeout(()=>{console.log("Let us a make a special Ice-Cream JUST FOR YOU!!")}
,0000);

// the above line will console.log later, even when we set time = 0, just b/s it is a-sync.

let order = (fruits_name,holder, topping, orderSize, call_production) => {
    
    // for(let i = 0;i<orderSize;i++){
    
    // }
    let totalFruit = parseInt(stocks.Fruits[fruits_name]) ;
    let totalHolder =  parseInt(stocks.holders[holder])  ;
    let totalMilk = parseInt(stocks.liquid.Milk)  ;
    let totalTopping = parseInt(stocks.toppings[topping])  ;
    
    let total = [totalFruit,totalHolder,totalMilk,totalTopping] ;
    

    // why Math.min(total) is not working ?
    let minimum = total.reduce((accumulator, current)=>{
        if(current<accumulator){
            accumulator = current;
        }
        return accumulator;
    },Infinity)


    if(totalFruit < orderSize || totalHolder < orderSize || totalMilk  < orderSize || totalTopping < orderSize){
        console.log("Sorry, Due to huge Demands, we can not process your Complete Order");
        
        console.log(`However, we can still provide you with ${minimum} Ice-Creams`)
        
        
    }
    
    
    if(stocks.Fruits.hasOwnProperty(fruits_name) && stocks.Fruits[fruits_name] > 0){
        console.log(`${fruits_name} flavour was selected !!`);
        stocks.Fruits[fruits_name] -= minimum;
        setTimeout(()=>{
            if(stocks.holders.hasOwnProperty(holder) && stocks.holders[holder] > 0){
                console.log(`${holder} was selected as Holder !!`);
                stocks.holders[holder] -= minimum;
                setTimeout(()=>{
                    if(stocks.toppings.hasOwnProperty(topping) && stocks.toppings[topping] > 0){
                        console.log(`${topping} was selected, You Really got a nice taste in Toppings !!`);
                        stocks.toppings[topping] -= minimum;
                        
                        setTimeout(()=>{
                            console.log("Sending Your soon to 'bae' Ice-Creams for Production..");
                            call_production(holder,topping,minimum);
                        },3000)
                        
                    }else{
                        console.log('Please Select a different Topping.. Sorry for inconvinience..');
                    }
                },1000)
            }else{
                console.log('Please Select a different Holder for your Ice-Cream .. Sorry for inconvinience..');
            }
        },1000)
    }else{
        console.log('Please Select a different Flavour.. Sorry for inconvinience..');
    }
    
    // If(flag) condition is executed first because it is in stack... queue will only be called if stack is empty... ?
    // need to read fcc blog..
    
    // setTimeout(()=>{
    //     if(stocks.Fruits.hasOwnProperty(fruits_name)){
    //         console.log(`${fruits_name} was selected !!`);
    //     }else{
    //         console.log('Please Select a different Fruit.. Sorry for inconvinience..');
    //         flag = false;
    //     }
    // },0);
    
    
};

let production = (holder, topping, minimum) => {
    setTimeout(()=>{
        console.log("Production started");
        
        setTimeout(()=>{
            console.log("Fruits cutting");
            
            setTimeout(()=>{
                console.log(`Milk and Ice added`);
                stocks.liquid.Milk -= minimum;
                
                setTimeout(()=>{
                    console.log("Machine has started");
                    
                    setTimeout(()=>{
                        console.log(`${holder} is selected as a Holder for the icecream`);
                        
                        setTimeout(()=>{
                            console.log(`${topping} are added on top of your delicious Ice-Cream.`);
                            
                            setTimeout(()=>{
                                console.log(`${minimum} IceCreama are Ready To be Served`);
                                setTimeout(()=>{
                                    console.log(" ;-) ");
                                    setTimeout(()=>{
                                        console.log("Enjoyyy..");
                                        
                                        console.log(stocks);
                                    },1000)
                                },4000)
                            },5000)
                        },1000);
                    },2000);
                },4000);
            },3000);
        },2000);
    },1000);
};

order("Banana","Cone","Dry Fruits",30,production);